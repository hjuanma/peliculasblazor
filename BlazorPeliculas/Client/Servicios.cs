﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorPeliculas.Client
{
   public class ServicioSingleton
   {
      public int valor { get; set; }
   }
   public class ServicioTransient
   {
      public int valor { get; set; }
   }
}
