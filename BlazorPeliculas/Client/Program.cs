using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using BlazorPeliculas.Client.Repos;

namespace BlazorPeliculas.Client
{
   public class Program
   {
      public static async Task Main(string[] args)
      {
         var builder = WebAssemblyHostBuilder.CreateDefault(args);
         builder.RootComponents.Add<App>("app");

         builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

         ConfigureService(builder.Services);

         await builder.Build().RunAsync();
      }

      //Metodo equivalente a la clase startup
      private static void ConfigureService(IServiceCollection services)
      {
         services.AddOptions(); //Para a�adir el sistema de autenticacion
         services.AddSingleton<ServicioSingleton>();
         services.AddTransient<ServicioTransient>();
         services.AddScoped<IRepo, Repositorio>();
      }
   }
}
