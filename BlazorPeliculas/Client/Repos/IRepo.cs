﻿using BlazorPeliculas.Shared.Entidades;
using System.Collections.Generic;

namespace BlazorPeliculas.Client.Repos
{
   public interface IRepo
   {
      List<Pelicula> ObtenerPeliculas();
   }
}
