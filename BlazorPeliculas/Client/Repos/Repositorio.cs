﻿using BlazorPeliculas.Shared.Entidades;
using System;
using System.Collections.Generic;

namespace BlazorPeliculas.Client.Repos
{
   public class Repositorio : IRepo
   {
      public List<Pelicula> ObtenerPeliculas()
      {

         return new List<Pelicula>
         {
            new Pelicula {Lanzamiento = DateTime.Now, Titulo = "Spider-Man" },
            new Pelicula {Lanzamiento = DateTime.Now, Titulo = "Spider-Man2" },
            new Pelicula {Lanzamiento = DateTime.Now, Titulo = "Spider-Man3" }
         };
      }
   }
}
