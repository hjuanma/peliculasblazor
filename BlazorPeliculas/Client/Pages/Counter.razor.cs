﻿using Microsoft.AspNetCore.Components;

namespace BlazorPeliculas.Client.Pages
{
   public class CounterBase : Microsoft.AspNetCore.Components.ComponentBase
   {

      [Inject] protected ServicioSingleton singleton { get; set; }
      [Inject] protected ServicioTransient transient { get; set; }

      protected int currentCount = 0;

      protected void IncrementCount()
      {
         // algo = ++ count (Primero suma despes iguala
         // algo = count ++ (Primero iguala despues suma)
         singleton.valor = transient.valor = ++currentCount;
      }
   }
}
